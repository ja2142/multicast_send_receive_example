#!/usr/bin/env python3

# multicast send/receive on one socket
# usage:
# run one or more times ./python_multicast_send_recv.py
# each instance will get random port from [9000, 9020),
# join the same multicast group, receive messages on it's port
# and send to ports [9000, 9020) in this group

import socket
import random
import time

MULTICAST_GROUP = '224.0.0.1'
MIN_PORT = 9000
MAX_PORT = 9020
LOCAL_PORT = random.randint(MIN_PORT, MAX_PORT)
# change to send and receive on different interface
# otherwise some default will be chosen (probably default from 'ip route list'?)
INTERFACE_ADDRESS = '0.0.0.0'

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# bind to any address (binding to some interface like 10.0.0.1 could prevent socket from receiving multicast traffic)
print(f'binding to :{LOCAL_PORT}')
sock.bind(('', LOCAL_PORT))

# INTERFACE_ADDRESS can limit interfaces from which multicast traffic will be accepted
# i.e.
#INTERFACE_ADDRESS='127.0.0.1'
mreq = socket.inet_aton(MULTICAST_GROUP) + socket.inet_aton(INTERFACE_ADDRESS)
print(f'setsockopt(IPPROTO_IP, IP_ADD_MEMBERSHIP, {MULTICAST_GROUP} + {INTERFACE_ADDRESS})')
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
if INTERFACE_ADDRESS != '0.0.0.0':
    # IP_ADD_MEMBERSHIP only filters received datagrams, to send from other interface than default
    # IP_MULTICAST_IF is needed
    print(f'setsockopt(IPPROTO_IP, IP_MULTICAST_IF, {INTERFACE_ADDRESS})')
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(INTERFACE_ADDRESS))

sock.setblocking(0)

print()

while True:
    while True:
        try:
            data, remote_endpoint = sock.recvfrom(1500)
            if len(data) > 0:
                print(f'received from {remote_endpoint}: {data}')
        except BlockingIOError:
            break
    print(f'sending to {MULTICAST_GROUP}:[{MIN_PORT},{MAX_PORT})')
    for remote_port in range(MIN_PORT, MAX_PORT):
        ret = sock.sendto(f'hello from {LOCAL_PORT}'.encode(), (MULTICAST_GROUP, remote_port))
    time.sleep(1)
